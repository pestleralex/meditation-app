const app = () => {
  const song = document.querySelector('.song');
  const play = document.querySelector('.play');
  const outline = document.querySelector('.moving-outline circle');
  const video = document.querySelector('.vid-container video');

  const sounds = document.querySelectorAll('.sound-picker button');
  const timeDisplay = document.querySelector('.time-display');
  const outlineLenght = outline.getTotalLength();

  let fakeDuration = 600;
  outline.style.strokeDasharray = outlineLenght;
  outline.style.strokeDashoffset = outlineLenght;

  play.addEventListener('click', () => {
    checkPlaying();
  });




  const checkPlaying = song => {
    if(song.paused){
      song.play();
      video.play();
      play.scr = './svg/pause.svg';
    } else {
      song.pause();
      video.pause();
      play.src = './svg/play.svg';
    }
  };
};


song.ontimeupdate = () =>{
  let currentTime = song.currentTime;
  let elapsed = fakeDuration-currentTime;
  let seconds = Match.floor(elapsed % 60);
  let minutes = Math.floor(elapsed/60)
}



app();
